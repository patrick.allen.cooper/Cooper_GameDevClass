﻿using UnityEngine;

public class AudioTimer : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    public float timer = 4.0f;
    int countdown = 3;
    public AudioSource[] countdownAudioArray;

    void Update()
    {
        timer -= Time.deltaTime;

        if (timer > 0.0f)
        {
            if (timer < (float)countdown)
            {
                GetComponent<AudioSource>().Play();
                countdown--;
            }
        }
        else
        {
            Debug.Log("AudioDone");
            timer = 40.0f;
            countdown = 30;
        }
    }
}

