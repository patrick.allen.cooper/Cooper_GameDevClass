﻿using UnityEngine;
using System.Collections;

public class FallIce : MonoBehaviour
{
    public GameObject IceStalactite;
    public float spawnTime = 1.5f;
    public float fallSpeed = 8.0f;
    public float spinSpeed = 250.0f;

    void Start()
    {
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }

    void Spawn()
    {
        int spawnPointX = Random.Range(-16, 35);
        int spawnPointY = 3;

        Vector3 spawnPosition = new Vector3(spawnPointX, spawnPointY, 0);
        Instantiate(IceStalactite, spawnPosition, Quaternion.identity);
    }

    void Update()
    {
        transform.Translate(Vector3.down * fallSpeed * Time.deltaTime, Space.World);
        transform.Rotate(Vector3.forward, spinSpeed * Time.deltaTime);
    }
}

