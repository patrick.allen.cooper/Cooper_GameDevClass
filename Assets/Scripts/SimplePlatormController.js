﻿public var speed : float;
public var gravity : float;
public var jumpSpeed : float;
private var facingRight = true;
private var verticalSpeed : float;
public var grounded : boolean;
public var doubleJump : boolean;
private var lastMoved : float;
private var currentTime : float;
public var stillFrames : int;

private var animator : Animator;

function Start () {
        grounded = false;
        doubleJump = false;
        animator = GetComponent(Animator);
        bored = false;
        stillFrames = 0;
}

function Update () {
		var h = Input.GetAxis("Horizontal");

        var delta = Input.GetAxis ("Horizontal") * speed;
        transform.position.x += delta * Time.deltaTime;

        // Set the Speed value based on delta and 
        // use this to animate walking.

        animator.SetFloat ("Speed", Mathf.Abs (delta));

        if (grounded) {
                if (Input.GetButton ("Jump")) {
                        animator.SetTrigger ("Jump");
                        grounded = false;
                        verticalSpeed = jumpSpeed;
                        if(Input.GetButton("Jump") && doubleJump == false){
                        	animator.SetTrigger ("Double");
                        	doubleJump = true;
                        	verticalSpeed = jumpSpeed;
                        }
                }
        } else {
                verticalSpeed -= gravity * Time.deltaTime;
                transform.position.y += verticalSpeed * Time.deltaTime;
        }
        
        // If we're not grounded we're probably falling...
        animator.SetBool ("Grounded", grounded);

        // We can use this to decide whether we're moving upward or
        // downward, could be used to animate jump vs fall
        animator.SetFloat ("VerticalSpeed", verticalSpeed);
}

function OnCollisionStay2D (other : Collision2D)
{
        verticalSpeed = 0;
        grounded = true;
        doubleJump = false;
}

function OnCollisionExit2D (other : Collision2D)
{
        grounded = false;
}

